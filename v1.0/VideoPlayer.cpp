#include "VideoPlayer.h"

extern "C"
{
#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/pixfmt.h"
#include "libswscale/swscale.h"
}
extern "C"
{
#pragma comment (lib, "Ws2_32.lib")    
#pragma comment (lib, "avcodec.lib")  
#pragma comment (lib, "avdevice.lib")  
#pragma comment (lib, "avfilter.lib")  
#pragma comment (lib, "avformat.lib")  
#pragma comment (lib, "avutil.lib")  
#pragma comment (lib, "swresample.lib")  
#pragma comment (lib, "swscale.lib")  
};

#include <stdio.h>
#include <iostream>
using namespace std;
VideoPlayer::VideoPlayer()
{
}
VideoPlayer::~VideoPlayer()
{
}
void VideoPlayer::startPlay()
{
	//调用 QThread 的start函数 将会自动执行下面的run函数 run函数是一个新的线程
	this->start();
}
//寻找最大元素所在位置
int FinaMaxIndex(int a[], int length)
{
	int max = a[0], index = 0, index_begin = 0, index_end = 0;
	for (int i = 0; i < length; i++)
	{
		if (a[i] > max)
		{
			max = a[i];
			index = i;
			//当连续几行或几列都为“满”光时，2017.12.7
			for (int j = 0; a[i + j] == max; j++)
				index = i + j / 2;
		}
		else
			continue;
	}
	return index;
}
//光源识别函数 ---2017.10.9
void Indentificate(QImage image, int width, int height)
{
	int threshold = 200;                     //二值化阈值

	static int temp[1280][720];              //光0-1矩阵,2017.10.10，static默认初始值为0
	int sum_hang[1280], sum_lie[720];	     //2017.10.10
											 //初始化sum值
	for (int i = 0; i < width; i++)
		sum_hang[i] = 0;
	for (int j = 0; j < height; j++)
		sum_lie[j] = 0;
	for (int i = 0; i < width; i++)
		for (int j = 0; j < height; j++)
		{
			QRgb rgb = image.pixel(i, j);
			int r = qRed(rgb) * 0.3;
			int g = qGreen(rgb) * 0.59;
			int b = qBlue(rgb) * 0.11;
			int rgb_all = r + g + b;        //先把图像灰度化，转化为灰度图像
			if (rgb_all > threshold)        //然后按某一阀值进行二值化
				temp[i][j] = 1;
			else
				temp[i][j] = 0;
			sum_hang[i] += temp[i][j];
			sum_lie[j] += temp[i][j];
		}
	//获取最大值所在位置---2017.10.10
	int maxIndex_hang = FinaMaxIndex(sum_hang, width);
	int	maxIndex_lie = FinaMaxIndex(sum_lie, height);
	//2017.10.11
	if (maxIndex_hang >= 0 && maxIndex_hang <= width && maxIndex_lie >= 0 && maxIndex_lie <= height)
		cout << maxIndex_hang << "  " << maxIndex_lie << endl;
}
void VideoPlayer::run()
{
	AVFormatContext *pFormatCtx;
	AVCodecContext *pCodecCtx;
	AVCodec *pCodec;
	AVFrame *pFrame, *pFrameRGB;
	AVPacket *packet;
	uint8_t *out_buffer;

	static struct SwsContext *img_convert_ctx;

	int videoStream, i, numBytes;
	int ret, got_picture;

	avformat_network_init();   //初始化FFmpeg网络模块，2017.8.5---lizhen
	av_register_all();         //初始化FFMPEG  调用了这个才能正常适用编码器和解码器

							   //Allocate an AVFormatContext.
	pFormatCtx = avformat_alloc_context();

	//2017.8.5---lizhen
	AVDictionary *avdic = NULL;
	char option_key[] = "rtsp_transport";
	char option_value[] = "tcp";
	av_dict_set(&avdic, option_key, option_value, 0);
	char option_key2[] = "max_delay";
	char option_value2[] = "100";
	av_dict_set(&avdic, option_key2, option_value2, 0);
	//rtsp地址，可根据实际情况修改
	//char url[]="rtsp://admin:admin@192.168.0.18:554/h264/ch1/main/av_stream";
	char url[] = "F:/master/Work_of_club/view_of_video/FangAn/FangAn2_FFMPEG/v2.0/FFmpeg_ChengPin_Identification_you/1.flv";

	if (avformat_open_input(&pFormatCtx, url, NULL, &avdic) != 0) {
		printf("can't open the file. \n");
		return;
	}

	if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
		printf("Could't find stream infomation.\n");
		return;
	}

	videoStream = -1;

	//循环查找视频中包含的流信息，直到找到视频类型的流
	//便将其记录下来 保存到videoStream变量中
	//这里我们现在只处理视频流，音频流先不管他
	for (i = 0; i < pFormatCtx->nb_streams; i++) {
		if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
			videoStream = i;
		}
	}

	//如果videoStream为-1 说明没有找到视频流
	if (videoStream == -1) {
		printf("Didn't find a video stream.\n");
		return;
	}

	//查找解码器
	pCodecCtx = pFormatCtx->streams[videoStream]->codec;
	pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
	//2017.8.9---lizhen
	pCodecCtx->bit_rate = 0;         //初始化为0
	pCodecCtx->time_base.num = 1;     //下面两行：一秒钟25帧
	pCodecCtx->time_base.den = 10;
	pCodecCtx->frame_number = 1;      //每包一个视频帧

	if (pCodec == NULL) {
		printf("Codec not found.\n");
		return;
	}

	//打开解码器
	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
		printf("Could not open codec.\n");
		return;
	}

	pFrame = av_frame_alloc();
	pFrameRGB = av_frame_alloc();

	//这里我们改成了 将解码后的YUV数据转换成RGB32
	img_convert_ctx = sws_getContext(pCodecCtx->width, pCodecCtx->height,
		pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height,
		AV_PIX_FMT_RGB32, SWS_BICUBIC, NULL, NULL, NULL);

	numBytes = avpicture_get_size(AV_PIX_FMT_RGB32, pCodecCtx->width, pCodecCtx->height);

	out_buffer = (uint8_t *)av_malloc(numBytes * sizeof(uint8_t));
	avpicture_fill((AVPicture *)pFrameRGB, out_buffer, AV_PIX_FMT_RGB32,
		pCodecCtx->width, pCodecCtx->height);

	int y_size = pCodecCtx->width * pCodecCtx->height;

	packet = (AVPacket *)malloc(sizeof(AVPacket)); //分配一个packet
	av_new_packet(packet, y_size);                  //分配packet的数据

	while (1)
	{
		if (av_read_frame(pFormatCtx, packet) < 0)
		{
			break;                                 //这里认为视频读取完了
		}

		if (packet->stream_index == videoStream) {
			ret = avcodec_decode_video2(pCodecCtx, pFrame, &got_picture, packet);

			if (ret < 0) {
				printf("decode error.\n");
				return;
			}

			if (got_picture) {
				sws_scale(img_convert_ctx,
					(uint8_t const * const *)pFrame->data,
					pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data,
					pFrameRGB->linesize);
				//把这个RGB数据 用QImage加载
				QImage tmpImg((uchar *)out_buffer, pCodecCtx->width, pCodecCtx->height, QImage::Format_RGB32);
				QImage image = tmpImg.copy();   //把图像复制一份 传递给界面显示
				QImage InImage = tmpImg.copy(); //识别图像，2017.10.9
				emit sig_GetOneFrame(image);    //发送信号

												//光源识别 ---2017.10.9
				int width = image.width(), height = image.height();
				//2017.8.11---lizhen
				//提取出图像中的R数据
				for (int i = 0; i<width; i++)
					for (int j = 0; j<height; j++)
					{
						QRgb rgb = image.pixel(i, j);
						int r = qRed(rgb);
						image.setPixel(i, j, qRgb(r, 0, 0));
					}
				emit sig_GetRFrame(image);
				//2017.10.9---在新线程中执行光源识别函数
				QFuture<void> future = QtConcurrent::run(Indentificate, InImage, width, height);
			}
		}
		av_free_packet(packet); //释放资源,否则内存会一直上升

								//2017.8.7---lizhen
		msleep(15); //停一停  不然放的太快了
	}
	av_free(out_buffer);
	av_free(pFrameRGB);
	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);
}

